import {Injectable} from '@angular/core';
import firebase from 'firebase';
import {Events} from "ionic-angular";
import {LanguageCodeProvider} from "../language-code/language-code";

/*
  Generated class for the GroupProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GroupProvider {
    fireGroupChats = firebase.database().ref('/groups');
    fireGroupMessagesChats = firebase.database().ref('/messagesGroup');
    allGroups;
    currGroup = {name: 'Bdfljlk', description: ''};
    currGroupMessages;

    constructor(public events: Events, public languageCode: LanguageCodeProvider) {
        console.log('Hello GroupProvider Provider');
    }

    inisilizeGroup(group) {
        this.currGroup = group;
    }

    addGroup(group) {
        this.fireGroupChats.child(group.name).set(group).then(() => {
            console.log('ok');
        }, (err) => {
            console.log(err)
        });
    }

    getAllGroups() {
        this.fireGroupChats.on('value', (snapshot) => {
            this.allGroups = [];
            let allGroupsAsObject = snapshot.val();
            for (let group in allGroupsAsObject) {
                this.allGroups.push(allGroupsAsObject[group]);
            }
            this.events.publish('allGroups');
        }, (e) => {
            console.log(e);
        });
    }

    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    formatDate(date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    }

    addnewmessage(msg, currUserLang) {
        let time = this.formatAMPM(new Date());
        let date = this.formatDate(new Date());
        let msgObj;
        let from = this.languageCode.getCodeByLanguage(currUserLang);
        let to = from === 'iw' ? 'en' : 'iw';
        if (this.currGroup) {
            var promise = new Promise((resolve, reject) => {
                this.languageCode.translateMsg(from, to, msg).then((data) => {
                    msgObj = {
                        sender: {
                            displayName: firebase.auth().currentUser.displayName,
                            uid: firebase.auth().currentUser.uid
                        },
                        msg: data.data
                    };
                    this.fireGroupMessagesChats.child(this.currGroup.name).push({
                        sentby: firebase.auth().currentUser.uid,
                        message: msgObj,
                        timestamp: firebase.database.ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date
                        // msgStatus:msgstatus.status
                    }).then(() => {
                        resolve(true);
                    })
                    // .catch((err) => {
                    //   reject(err);
                    // })
                }, (err) => {
                    reject(err)
                });
            });
            // })
            return promise;
        }
    }

    getGroupMessages() {
        let temp;
        this.fireGroupMessagesChats.child(this.currGroup.name).on('value', (snapshot) => {
            this.currGroupMessages = [];
            temp = snapshot.val();

            for (let tempkey in temp) {
                this.currGroupMessages.push(temp[tempkey]);
            }
            this.events.publish('groupNewMessage');
        })
    }
}
