import {Injectable} from '@angular/core';
import axios from "axios";

/*
  Generated class for the LanguageCodeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LanguageCodeProvider {
    constructor() {

    }

    allCodes = {
        "English": "en",
        "Hebrew": "iw"
    };

    getCodeByLanguage(language) {
        return this.allCodes[language];
    }

    translateMsg(from, to, msg) {
        return axios.post('https://kitapptranslate.herokuapp.com/translate', {from: from, to: to, msg: msg});
    }
}
