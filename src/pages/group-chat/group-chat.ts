import {Component, ViewChild} from '@angular/core';
import {Content, Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {GroupProvider} from "../../providers/group/group";
import {UserProvider} from "../../providers/user/user";
import {LanguageCodeProvider} from "../../providers/language-code/language-code";

/**
 * Generated class for the GroupChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-group-chat',
    templateUrl: 'group-chat.html',
})
export class GroupChatPage {
    @ViewChild('content') content: Content;
    group;
    newmessage = '';
    groupMessages = [];
    currUser;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public userservice: UserProvider,
                public groupService: GroupProvider,
                public languageservice: LanguageCodeProvider,
                public events: Events) {
        this.group = this.groupService.currGroup;
        this.scrollto();
        this.groupService.getGroupMessages();
        this.events.subscribe('groupNewMessage', () => {
            this.groupMessages = this.groupService.currGroupMessages;
        });
        this.loaduserdetails();
    }

    scrollto() {
        setTimeout(() => {
            this.content.scrollToBottom();
        }, 1000);
    }

    addmessage() {
        this.groupService.addnewmessage(this.newmessage, this.currUser.language).then(() => {
            this.content.scrollToBottom();
            this.newmessage = '';
        })
    }

    getLanguageCode(userLan, msg) {
        let code = this.languageservice.getCodeByLanguage(userLan);
        return msg[code];
    }

    loaduserdetails() {
        this.userservice.getuserdetails().then((res: any) => {
            this.currUser = res;
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad GroupChatPage');
        this.group = this.groupService.currGroup;
        this.groupService.getGroupMessages();
        this.events.subscribe('groupNewMessage', () => {
            this.groupMessages = this.groupService.currGroupMessages;
        });
    }
}
