import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

    group = {name: '', description: ''};
    constructor(public navCtrl: NavController, public viewCtrl : ViewController ,public navParams: NavParams) {
    }
    closeModal(){
        this.viewCtrl.dismiss();
    }

    create() {
        this.viewCtrl.dismiss(this.group);
    }


    ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

}
